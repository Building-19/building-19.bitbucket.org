const gulp = require('gulp');
const rename = require('gulp-rename');
const insert = require('gulp-insert');

gulp.task('default', function(){
  gulp.src('index.md')
  .pipe(rename('index.html'))
  .pipe(insert.prepend('<!DOCTYPE html><html><title>Truth and Grace</title><xmp theme="Journal" style="display:none;">'))
  .pipe(insert.append('</xmp><script src="http://strapdownjs.com/v/0.2/strapdown.js"></script></html>'))
  .pipe(gulp.dest('./'));
});
